import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from "@material-ui/core"
import logors from "../../img/logors.png"
import api from "../../service/api.js";
import { useUserContext } from "../../contexts/createdUser";

const useStyles = makeStyles((theme) => ({
    root: {
        background: 'linear-gradient(160deg, #FFF 20%, #0000FF 80%)',
        borderRadius: 3,
        border: 0,
        color: '#09a100',
        height: 160,
        padding: '0 30px',
        boxShadow: '0 3px 5px 2px rgba(0, 105, 135, .3)',
      },
      label: {
        textTransform: 'capitalize',
      },
    }));

export default function Resposta() {
  const { user } = useUserContext();
  const [ vacinacao, setVacinacao ] = useState(false);
  
  useEffect(() => {
    const fetchVacinacao =  () => {
      const { email } = user
      if (email) {
        api.vacinacao(email).then((resp) => {
          setVacinacao(resp);
          console.log(vacinacao);
        });
      }
    }
    fetchVacinacao();
  },)

  return (
    <div>
      <Typography height="auto" align="center" variant="h5">
        {vacinacao ? "Você já pode se vacinar!" : "Você não pode se vacinar ainda!"}
      </Typography>
      <div className="logo">
        <img src={logors} alt="logors"  height="150px"></img>
      </div>
    </div>
  );
}
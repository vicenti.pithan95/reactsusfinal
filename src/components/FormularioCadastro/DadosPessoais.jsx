import React, { useState, useEffect } from "react";
import { useValidacoesCadastro } from '../../contexts/validacoesCadastro'
import useErros from "../../hooks/useErros";
import {Button, TextField } from '@material-ui/core';
import { useUserContext } from "../../contexts/createdUser"

export default function DadosPessoais({ proximo }) {
  const [nome, setNome] = useState("");
  const [nascimento, setNascimento] = useState("");
  const [email, setEmail] = useState("");
  const validacoes = useValidacoesCadastro();
  const [erros, validarCampos, possoEnviar] = useErros(validacoes);

  const user = useUserContext();

  useEffect(() => {
    console.log(user);
  }, [user])

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        if (possoEnviar()) {
          user.setUser({ nome, email, nascimento, });
          proximo();
        }
      }}
    >
      <TextField
        value={nome}
        onChange={(event) => {
          setNome(event.target.value);
        }}
        onBlur={validarCampos}
        error={!erros.nome.valido}
        helperText={erros.nome.texto}
        id="nome"
        name="nome"
        label="Nome"
        variant="outlined"
        margin="normal"
        fullWidth
        required
      />
      <TextField
        value={email}
        onChange={(event) => {
          setEmail(event.target.value);
        }}
        onBlur={validarCampos}
        error={!erros.email.valido}
        helperText={erros.email.texto}
        id="email"
        name="email"
        label="email"
        type="email"
        variant="outlined"
        margin="normal"
        fullWidth
        required
      />
      <TextField
        value={nascimento}
        onChange={(event) => {
          setNascimento(event.target.value);
        }}
        id="nascimento"
        label="Data de Nascimento"
        type="date"
        margin="normal"
        variant="outlined"
        fullWidth
        required
        InputLabelProps={{
          shrink: true,
        }}
      />

      <Button  margin="normal" variant="contained" color="primary" type="submit">
        Próximo
      </Button>
    </form>
  );
}
const url ="http://testesusrs-001-site1.htempurl.com";
function comorbidade() {
  return fetch(
    url + "/api/grupoespecial/comorbidade/true",
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let grupos = response.json();
    return grupos.then(list=>{
      return list.map(({ id, nmGrupo }) => ({
        id,
        nmGrupo,
      }));
    })
  });
}
function grupo() {
  return fetch(
    url + "/api/grupoespecial/comorbidade/false",
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let grupos = response.json();
    return grupos.then(list=>{
      return list.map(({ id, nmGrupo }) => ({
        id,
        nmGrupo,
      }));
    })
  });
}
function grupoTotal() {
  const headers = new Headers();
  headers.append("Content-Type", "application/json")
  headers.append("Authorization", "Bearer " + localStorage.getItem('app-token'));
  return fetch(
    url + "/api/grupoespecial",
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let grupos = response.json();
    return grupos.then(list=>{
      return list.map(({ id, nmGrupo, isComorbidade }) => ({
        id,
        nmGrupo,
        isComorbidade,
      }));
    })
  });
}
function local() {
  return fetch(
    url + "/api/local",
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let resp = response.json();
    return resp.then(cidades=>{
      return cidades.map(({ id, nmCidade }) => ({
        id,
        nmCidade,
      }));
    })
  });
}
function listaCidadaos() {
  const headers = new Headers();
  headers.append("Content-Type", "application/json")
  headers.append("Authorization", "Bearer " + localStorage.getItem('app-token'));
  return fetch(
    url + "/api/cidadao",
    {
      method: "GET",
      headers,
      mode: "cors",
      credentials: "omit",
    }
  ).then((response) =>{
    let resp = response.json();
    return resp
  });
}
function emailDuplicado(email) {
    return fetch(
      url + "/api/cidadao/email/" + email,
      {
        method: "GET",
        mode: "cors",
        credentials: "omit",
      }
    ).then( resp => resp.json());
};

async function vacinacao(email) {
  return fetch(
    url + "/api/cidadao/vacinacao/" + email,
    {
      method: "GET",
      mode: "cors",
      credentials: "omit",
    }
  ).then( resp => resp.json());
};

function save(data) {
  return fetch(
    url + "/api/cidadao/",
    {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json"},
      credentials: "omit",
      body: JSON.stringify(data)
    }
  ).then( resp => resp.json()); 
}
async function login(dados) {
  const response = await fetch(
    url + "/api/account/login",
    {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json"},
      credentials: "omit",
      body: JSON.stringify(dados)
    }
  );
  const { token } = await response.json()
     if(token){
       localStorage.setItem('app-token', token);
       return !!localStorage.getItem('app-token')
     }
}

async function createLocal(dados) {
  const headers = new Headers();
  headers.append("Content-Type", "application/json")
  headers.append("Authorization", "Bearer " + localStorage.getItem('app-token'));
  const response = await fetch(
    url + "/api/local",
    {
      method: "POST",
      mode: "cors",
      headers,
      credentials: "omit",
      body: JSON.stringify(dados)
    }
  );
  const local = await response.ok
  console.log(local)
  return local;
}

async function createGrupoEspecial(dados) {
  const headers = new Headers();
  headers.append("Content-Type", "application/json")
  headers.append("Authorization", "Bearer " + localStorage.getItem('app-token'));
  const response = await fetch(
    url + "/api/grupoespecial",
    {
      method: "POST",
      mode: "cors",
      headers,
      credentials: "omit",
      body: JSON.stringify(dados)
    }
  );
  const grupo = await response.ok
  console.log(grupo)
  return grupo;
}

async function createVacinacao(dados) {
  const headers = new Headers();
  headers.append("Content-Type", "application/json")
  headers.append("Authorization", "Bearer " + localStorage.getItem('app-token'));
  const response = await fetch(
    url + "/api/vacinacao",
    {
      method: "POST",
      mode: "cors",
      headers,
      credentials: "omit",
      body: JSON.stringify(dados)
    }
  );
  const vacinacao = await response.ok
  console.log(vacinacao)
  return vacinacao;
}

const api = { emailDuplicado, save, vacinacao, local, grupo, comorbidade, 
  login, createLocal, createGrupoEspecial, grupoTotal, createVacinacao,listaCidadaos };
export default api;
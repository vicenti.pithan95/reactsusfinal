import React, { useContext } from 'react';
import {validarEmail, validarNome} from '../models/cadastro'

const validacoesPadrao = {
  senha: semValidacao,
  nome: validarNome,
  email: validarEmail
}

const ValidacoesCadastro = React.createContext(validacoesPadrao);

function semValidacao(dados) {
    return {valido: true, texto: ""}
}

export function ValidacoesCadastroProvider({children}) {
    return <ValidacoesCadastro.Provider value={validacoesPadrao} >
      {children}
    </ValidacoesCadastro.Provider>
}

export function useValidacoesCadastro() {
  return useContext(ValidacoesCadastro);
};

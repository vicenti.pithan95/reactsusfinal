import api from "../service/api.js"
function validarNome(nome){
  if(nome.length < 4 || nome.length >16){
    return {valido:false, texto:"Nome deve ter 4 e 16 dígitos."}
  }else{
    return { valido: true, texto: "" };
  }
}

async function validarEmail(email){
  let isDuplicado = await api.emailDuplicado(email);
  if(isDuplicado){
    return {valido:false, texto:"Este email já está cadastrado."}
  }else{
    return { valido: true, texto: "" };
  }
}

export { validarNome, validarEmail };
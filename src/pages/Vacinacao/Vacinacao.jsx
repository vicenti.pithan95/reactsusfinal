import React, { useState, useEffect } from "react";
import {Button, TextField, Container, Typography, MenuItem} from '@material-ui/core'; 
import api from "../../service/api"
import { useHistory } from "react-router-dom";
import {Alert} from '@material-ui/lab'
const useStyles ={
  margin: "15% 0",
}
const useStylesBt ={
  margin: "1% 1%",
}
function Vacinacao() {
  const history = useHistory();
  const [idade, setIdade] = useState(18);
  const [cidades, setCidades] = useState([]);
  const [localSelecionado, setLocalSelecionado] = useState("");
  const [grupos, setGrupos] = useState([]);
  const [mensagemAlerta, setMensagemAlerta] = useState("error");
  const [grupoSelecionado, setGrupoSelecionado] = useState("");
  const [tipoAlerta, setTipoAlerta] = useState("error");
  const [visivel, setVisivel] = useState(false);
  useEffect(() => {
    const buscarCampos = 
      async () => {
        await Promise.all([
          api.local().then(e=>{
          setCidades(e)
          }),
          api.grupoTotal().then(e=>{
            setGrupos(e)
          })
        ]);
      }
      buscarCampos();
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setVisivel(false);
    }, 3000);
  }, [visivel]);

  const grupoChange = (event) => {
    setGrupoSelecionado(event.target.value);
  };
  const localChange = (event) => {
    setLocalSelecionado(event.target.value);
  };
  return (
    <Container component="article" maxWidth="sm">
      <div style={useStyles}></div>
      {visivel && (
        <Alert severity={tipoAlerta} style={{ marginBottom: "30px" }}>
          {mensagemAlerta}
        </Alert>
      )}
      <Typography height="auto" align="center" variant="h5">
        Adicionar Critério de Vacinação
      </Typography>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          console.log({ idade, localSelecionado, grupoSelecionado });
          const dados = {
            idadeCorte: parseInt(idade),
            local: {
              id: localSelecionado,
            },
            grupoEspecial: {
              id: grupoSelecionado,
            },
          };
          let save = false;
          async function saving() {
            save = await api.createVacinacao(dados);
            return save;
          }
          saving().then((resp) => {
            setVisivel(true);
            setTipoAlerta("success");
            setMensagemAlerta("Vacinação salva com sucesso !");
            console.log(resp);
            history.push("/menu/vacinacao");
          });
          
        }}
      >
        <TextField
          value={idade}
          onChange={(event) => {
            setIdade(event.target.value);
          }}
          id="idade"
          name="idade"
          label="Idade de Corte"
          type="number"
          variant="outlined"
          margin="normal"
          fullWidth
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          select
          value={localSelecionado}
          onChange={localChange}
          label="Cidade"
        >
          {cidades.map((local) => (
            <MenuItem key={local.id} value={local.id}>
              {local.nmCidade}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          select
          value={grupoSelecionado}
          onChange={grupoChange}
          label="Grupo Especial"
        >
          {grupos.map((grupo) => (
            <MenuItem key={grupo.id} value={grupo.id}>
              {grupo.nmGrupo}
            </MenuItem>
          ))}
        </TextField>
        <Button type="submit" variant="contained" color="primary">
          Adicionar
        </Button>
        <Button
          style={useStylesBt}
          variant="contained"
          color="primary"
          onClick={() => {
            history.push("/menu");
          }}
        >
          voltar
        </Button>
      </form>
    </Container>
  );
}

export default Vacinacao;
import React, { useState } from "react";
import { useValidacoesCadastro }  from "../../contexts/validacoesCadastro";
import {Button, TextField, Container, Typography } from '@material-ui/core';
import useErros from "../../hooks/useErros.js";
import { useHistory } from "react-router-dom";
import api from "../../service/api"
const useStyles ={
  margin: "1% 1%",
}
function Login() {
  const history = useHistory();
  const [nome, setNome] = useState("");
  const [senha, setSenha] = useState("");
  const validacoes = useValidacoesCadastro();
  const [erros, validarCampos] = useErros(validacoes);
  function possoEnviar() {
    for (let campo in erros) {
      if (!erros[campo].valido) {
        return false;
      }
    }
    const data = {
      username: nome,
      password: senha
    }

    api.login(data).then(success =>{
         if(success){
           history.push("/menu")
         }
        })
    }
  return (
    <Container component="article" maxWidth="sm">
      <Typography height="auto" align="center" variant="h5">
        Menu Administrativo
      </Typography>
      <form
        onSubmit={(event) => {
          event.preventDefault();
         possoEnviar()
        }}
      >
        <TextField
          value={nome}
          onChange={(event) => {
            setNome(event.target.value);
          }}
          id="nome"
          name="nome"
          label="nome"
          type="text"
          variant="outlined"
          margin="normal"
          fullWidth
          required
        />
        <TextField
          value={senha}
          onChange={(event) => {
            setSenha(event.target.value);
          }}
          onBlur={validarCampos}
          error={!erros.senha.valido}
          helperText={erros.senha.texto}
          id="senha"
          name="senha"
          label="senha"
          type="password"
          variant="outlined"
          margin="normal"
          fullWidth
          required
        />
        <Button type="submit" variant="contained" color="primary">
          Acessar
        </Button>
          <Button
            style= {useStyles}
            variant="contained"
            color="primary"
            onClick={() => {
              history.push("/");
            }}
          >
            voltar
          </Button>
      </form>
    </Container>
  );
}

export default Login;
import React, { useState, useEffect } from "react";
import {Button, TextField, Container, Typography, MenuItem} from '@material-ui/core';
import cidadesRs from "../../service/Ibge.js";
import { useHistory } from "react-router-dom";
import api from "../../service/api"
import {Alert} from '@material-ui/lab'
const useStyles ={
  margin: "15% 0",
}
const useStylesBt ={
  margin: "1% 1%",
}
function Local() {
  const history = useHistory();
  const [nomeUf] = useState("Rio Grande do Sul");
  const [siglaUf] = useState("RS");
  const [cidades, setCidades] = useState([]);
  const [mensagemAlerta, setMensagemAlerta] = useState("error");
  const [tipoAlerta, setTipoAlerta] = useState("error");
  const [visivel, setVisivel] = useState(false);
  const [cidadeSelecionada, setCidadeSelecionada] = useState("");
  useEffect(() => {
    cidadesRs().then((e) => {
      setCidades(e);
    });
  }, []);
  useEffect(() => {
    setTimeout(() => {
      setVisivel(false);
    }, 3000);
  }, [visivel]);
  const handleChange = (event) => {
    setCidadeSelecionada(event.target.value);
  };
  return (
    <Container component="article" maxWidth="sm">
      <div style={useStyles}></div>
      {visivel && (
        <Alert severity={tipoAlerta} style={{ marginBottom: "30px" }}>
          {mensagemAlerta}
        </Alert>
      )}
      <Typography height="auto" align="center" variant="h5">
        Adicionar Cidade
      </Typography>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          const dados = {
            nmUf: nomeUf,
            siglaUf: siglaUf,
            nmCidade: cidadeSelecionada,
          };
          async function saving(save) {
            let resp = await api.createLocal(dados);
            return resp;
          }
            saving().then((resp) => {
              console.log("no if o save é " + resp);
              setVisivel(true);
              if (resp) {
                setTipoAlerta("success");
                setMensagemAlerta("Local salvo com sucesso !");
              } else {
                setTipoAlerta("error");
                setMensagemAlerta("Local já cadatrado!");
              }
            });
        }}
      >
        <TextField
          value={nomeUf}
          id="Rs"
          name="uf"
          label="Rio Grande do Sul"
          type="text"
          variant="outlined"
          margin="normal"
          disabled={true}
          fullWidth
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          select
          value={cidadeSelecionada}
          onChange={handleChange}
          label="Cidade"
        >
          {cidades.map((cidade) => (
            <MenuItem key={cidade.id} value={cidade.nome}>
              {cidade.nome}
            </MenuItem>
          ))}
        </TextField>
        <Button
          type="submit"
          variant="contained"
          color="primary"
        >
          Adicionar
        </Button>
        <Button
          style={useStylesBt}
          variant="contained"
          color="primary"
          onClick={() => {
            history.push("/menu");
          }}
        >
          voltar
        </Button>
      </form>
    </Container>
  );
}

export default Local;
import React from "react";
import FormularioCadastro from "../../components/FormularioCadastro/FormularioCadastro";
import { Container, Button } from "@material-ui/core";
import logo from "../../img/logo.png"
import { ValidacoesCadastroProvider } from "../../contexts/validacoesCadastro";
import { validarEmail,validarNome } from "../../models/cadastro";
import { useHistory } from "react-router-dom";
const useStyles ={
  margin: "0% 0%",
}
function Home() {
  const history = useHistory();
  
  return (
    <>
      <Container component="article" maxWidth="sm">
        <div className="logo">
          <img src={logo} alt="logo" align="center" height="180px"></img>
        </div>
        <ValidacoesCadastroProvider
          value={{ email: validarEmail, nome: validarNome }}
        >
          <Button style={useStyles} variant="contained" color="primary" onClick={()=>{history.push("/login")}}>institucional</Button>
          <FormularioCadastro />
        </ValidacoesCadastroProvider>
      </Container>
    </>
  );
}

export default Home;